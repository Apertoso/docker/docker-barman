FROM debian:bookworm-slim

RUN set -x \
    && apt-get update \
    && apt-get install -y --no-install-recommends \
        wget \
        cron \
        ca-certificates \
        gosu \
        gnupg2 \
        python3 \
        python3-prometheus-client \
    && apt-get clean

# Add Tini so defunct child processes get reaped properly
ENV TINI_VERSION v0.19.0
ADD https://github.com/krallin/tini/releases/download/${TINI_VERSION}/tini-static-amd64 /tini
RUN chmod +x /tini

# TODO: check debian default crontab file: creates defunct processes
RUN set -x \
    && sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt/ bookworm-pgdg main" > /etc/apt/sources.list.d/pgdg.list' \
    && wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add - \
    && apt-get update \
    && apt-get install -y --no-install-recommends \
        postgresql-client \
        barman \
    && apt-get clean \
    && echo ok

# grab gosu for easy step-down from root
ENV GOSU_VERSION 1.16
RUN set -x \
    && wget -qO /usr/local/bin/gosu "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$(dpkg --print-architecture)" \
    && wget -qO /usr/local/bin/gosu.asc "https://github.com/tianon/gosu/releases/download/$GOSU_VERSION/gosu-$(dpkg --print-architecture).asc" \
    && export GNUPGHOME="$(mktemp -d)" \
    && gpg --keyserver keys.openpgp.org --recv-keys B42F6819007F00F88E364FD4036A9C25BF357DD4 \
    && gpg --batch --verify /usr/local/bin/gosu.asc /usr/local/bin/gosu \
    && rm -rf "$GNUPGHOME" /usr/local/bin/gosu.asc \
    && chmod +x /usr/local/bin/gosu \
    && gosu nobody true


ENV DOCKERIZE_VERSION v0.6.1
RUN wget -q https://github.com/jwilder/dockerize/releases/download/$DOCKERIZE_VERSION/dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && tar -C /usr/local/bin -xzvf dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz \
    && rm dockerize-linux-amd64-$DOCKERIZE_VERSION.tar.gz

VOLUME /etc/barman.d/
VOLUME /var/lib/barman/

ENV BARMAN_LOG_FILE=/var/log/barman.log \
    BARMAN_BARMAN_HOME=/var/lib/barman \
    BARMAN_BARMAN_LOCK_DIRECTORY=/tmp \
    BARMAN_CONFIGURATION_FILES_DIRECTORY=/etc/barman.d \
    BARMAN_PRE_BACKUP_SCRIPT=/opt/barman/scripts/pre_backup.sh \
    BARMAN_POST_BACKUP_SCRIPT=/opt/barman/scripts/post_backup.sh \
    PROM_EXPORTER_LOG_FILE="/var/log/barman_prom_exporter.log"

COPY scripts /opt/barman/scripts
COPY docker-entrypoint.sh /
COPY group passwd /etc/

EXPOSE 8000

WORKDIR $BARMAN_BARMAN_HOME

ENTRYPOINT ["/tini", "--", "/docker-entrypoint.sh"]

CMD ["barman"]